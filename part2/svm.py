""" Generates the model for the given training data for SVM-Light Classifier """

__author__ = 'Shank'

import re
from collections import OrderedDict
from math import log


# To get the learning file format (key=value)
def printable_string(word_dict):
    return "".join('{}:{} '.format(k, v) for k, v in word_dict.items())

# Read training data
training_file = open("/Users/Shank/Desktop/NLP - HW1/Spam/TrainingFile-Spam", 'r')

# Model file path
model_file = open("/Users/Shank/Desktop/NLP - HW1/Spam/spam.svm.model", 'w')

local_line_data = dict()
line_data = dict()
local_term_freq = dict()
inv_doc_freq = dict()
local_inv_doc_freq = set()
word_key = dict()

class_assignment = {"HAM": "+1", "SPAM": "-1", "POS": "+1", "NEG": "-1"}
learning = ""
line_count = 0
i = 1

lines = training_file.readlines()
training_file.close()

for line in lines:

    local_term_freq = dict()
    local_inv_doc_freq = set()
    local_line_data = dict()

    words = re.findall(r'\w+', line.lower())

    local_line_data["class"] = class_assignment[words[0].upper()]
    words.remove(words[0])
    local_line_data["words.count"] = len(words)

    # Re-initialize for each document
    for word in words:

        # Calculate the TF for a document
        if word in local_term_freq:
            local_term_freq[word] += 1
        else:
            local_term_freq[word] = 1

        # Assign an unique number for each word
        if word not in word_key:
            word_key[word] = i
            i += 1

        # Calculate the IDF for a document
        local_inv_doc_freq.add(word)

    for word in local_inv_doc_freq:
        if word in inv_doc_freq:
            inv_doc_freq[word] += 1
        else:
            inv_doc_freq[word] = 1

    local_line_data["term.freq"] = local_term_freq
    line_data[line_count] = local_line_data
    line_count += 1


# Generate model file content
for idx in range(0, line_count):
    line = line_data[idx]
    learning += line["class"] + " "

    termFreq = line["term.freq"]
    wordsCount = line["words.count"]

    toPrint = dict()

    for word in termFreq:
        toPrint[word_key[word]] = (termFreq[word]/wordsCount)*(log(len(lines)/inv_doc_freq[word]))

    toPrint = OrderedDict(sorted((int(key), value) for key, value in toPrint.items()))
    learning += printable_string(toPrint) + "\n"

# Generate model file
model_file.write(learning)
model_file.close()


#----------------------------------#

local_line_data = dict()
line_data = dict()
local_term_freq = dict()
inv_doc_freq = dict()
local_inv_doc_freq = set()

learning = ""
line_count = 0
i = 1


# Read training data
training_file = open("/Users/Shank/Desktop/NLP - HW1/Spam/TestFile-Spam", 'r')

# Model file path
model_file = open("/Users/Shank/Desktop/NLP - HW1/Spam/svm.test", 'w')

lines = training_file.readlines()
training_file.close()

for line in lines:

    local_term_freq = dict()
    local_inv_doc_freq = set()
    local_line_data = dict()

    words = re.findall(r'\w+', line.lower())

    #local_line_data["class"] = class_assignment[words[0].upper()]
    words.remove(words[0])
    local_line_data["words.count"] = len(words)

    # Re-initialize for each document
    for word in words:

        # Calculate the TF for a document
        if word in local_term_freq:
            local_term_freq[word] += 1
        else:
            local_term_freq[word] = 1

        # Assign an unique number for each word
        if word not in word_key:
            word_key[word] = i
            i += 1

        # Calculate the IDF for a document
        local_inv_doc_freq.add(word)

    for word in local_inv_doc_freq:
        if word in inv_doc_freq:
            inv_doc_freq[word] += 1
        else:
            inv_doc_freq[word] = 1

    local_line_data["term.freq"] = local_term_freq
    line_data[line_count] = local_line_data
    line_count += 1


# Generate model file content
for idx in range(0, line_count):
    line = line_data[idx]
    #learning += line["class"] + " "

    termFreq = line["term.freq"]
    wordsCount = line["words.count"]

    toPrint = dict()

    for word in termFreq:
        toPrint[word_key[word]] = (termFreq[word]/wordsCount)*(log(len(lines)/inv_doc_freq[word]))

    toPrint = OrderedDict(sorted((int(key), value) for key, value in toPrint.items()))
    learning += printable_string(toPrint) + "\n"

# Generate model file
model_file.write(learning)
model_file.close()