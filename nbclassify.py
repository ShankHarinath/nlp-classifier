""" Naive Bayes Classifier """

__author__ = 'Shank'

import codecs
import re
import sys

probabilities = dict()
content = ""
maxProb = dict()
final_class = ""
fileName = sys.argv[2]


# Reads the probabilities from the model file
def read_probabilities(file_name):
    with open(file_name, 'r') as f:
        for row in f:
            row = row.rstrip()
            if row == "":
                continue
            key, value = row.split("=", 1)
            probabilities[key] = value


read_probabilities(sys.argv[1])
input_file = codecs.open(fileName, "r", encoding='ISO-8859-1')

classes = str(probabilities["classes"]).split(",")
lines = input_file.readlines()

# Repeat for line/document
for line in lines:

    line = line.rstrip().replace("'", "")

    # Repeat for each class
    for nlp_class in classes:

        # Prior of the class
        prob = float(probabilities[nlp_class + ".sentCount"])

        # Finding conditional for each word given the class
        #words = re.findall('[A-Z]{2,}(?![a-z])|[A-Z][a-z]+(?=[A-Z])|[\'\w\-]+', line.lower())
        words = re.findall(r'\w+', line.lower().replace("'", ""))

        for word in words:
            if nlp_class + "." + word in probabilities:
                prob += float(probabilities[nlp_class + "." + word])
            else:
                prob += float(probabilities[nlp_class + ".unknown"])

        maxProb[nlp_class] = prob

    v = list(maxProb.values())
    k = list(maxProb.keys())
    print(k[v.index(max(v))])