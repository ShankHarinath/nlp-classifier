__author__ = 'Shank'


import sys


f1 = open(sys.argv[1], 'r')
f2 = open(sys.argv[2], 'r')

actual_list = list()
predicted_list = list()
classes = set()

for line1 in f1:
    actual_list.append(line1.strip('\r\n'))

for line2 in f2:
    predicted_list.append(line2.strip('\r\n'))
    classes.add(line2.strip('\r\n'))

#print(classes)

tp = 0
tn = 0
fp = 0
count = 0

for each_class in classes:
    for x in range(0, len(actual_list)):
        if predicted_list[x] == each_class:
            if actual_list[x] == each_class:
                tp += 1
            else:
                tn += 1
        elif predicted_list[x] != each_class:
            if actual_list[x] == each_class:
                fp += 1

    print(each_class)
    precision = tp / (tp+tn)
    recall = tp / (tp+fp)
    tp = 0
    tn = 0
    fp = 0
    #print(precision)
    #print(recall)
    fscore = (2*precision*recall)/(precision+recall)
    print(fscore)