# Part I #

# Spam Detection #
###SPAM###

* Precision = 0.9574468085106383
* Recall = 0.9917355371900827
* F-score = 0.9742895805142084

###HAM###

* Precision = 0.9969604863221885
* Recall = 0.984
* F-score = 0.9904378459989935

# Sentiment Analysis #

###POS###

* Precision = 0.8728668941979523
* Recall = 0.8475559237779619
* F-score - 0.8600252206809583

###NEG###

* Precision = 0.8614457831325302
* Recall = 0.8847641144624904
* F-score = 0.8729492560091568

# Part II #

#Spam Detection #
#SVM#
### SPAM###

* Precision = 0.9692737430167597
* Recall = 0.9559228650137741
* F-score = 0.9625520110957004

###HAM###

* Precision = 0.9840796019900497
* Recall = 0.989
* F-score = 0.9865336658354115

#MegaM #
###SPAM###

* Precision = 0.9530201342281879
* Recall = 0.7823691460055097
* F-score = 0.859304084720121

###HAM###

* Precision = 0.9258215962441314
* Recall = 0.986
* F-score = 0.9549636803874091

#Sentiment Analysis #
#SVM #
###POS###

* Precision = 0.8961881589618816
* Recall = 0.9154929577464789
* F-score = 0.905737704918033

###NEG###

* Precision = 0.9194948697711128
* Recall = 0.9010054137664346
* F-score = 0.91015625

#MegaM#
###POS###

* Precision = 0.7892307692307692
* Recall = 0.8500414250207126
* F-score = 0.8185081771041085

###NEG###

* Precision = 0.8491666666666666
* Recall = 0.7880897138437741
* F-score = 0.8174889691135178

#What happens exactly to precision, recall and F-score when only 10% of the training data is used to train the classifiers?#

* The precision, recall and F-score should go down given less training data. But the scores in this case do not change considerably. The reason could be that most of the features in the test set are unknown and it acts as a neutral feature and adds the same amount to both the classes. This makes the known features very critical in deciding the class of the document. We have conditional probability for the unknown words as we have usd Add-One Smoothing.
