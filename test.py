""" Test File """

__author__ = 'Shank'

inputFile = open("/Users/Shank/Documents/NLP/SVM/predictions", 'r')
output = open("/Users/Shank/PycharmProjects/NBClassifier/part2/spam.svm.out", 'w')
learning = ""

lines = inputFile.readlines()
inputFile.close()

for line in lines:
    if float(line) > 0:
        learning += "HAM\n"
    else:
        learning += "SPAM\n"

output.write(learning)
output.close()