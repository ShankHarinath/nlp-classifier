""" Generates the training data for the Naive Bayes Classifier """
__author__ = 'Shank'

import os
import codecs
import re

# Read input directory name
#directory = sys.argv[1]
directory = "/Users/Shank/Documents/NLP/SENTIMENT_test"
training_file = "/Users/Shank/Desktop/NLP - HW1/MegaM/Senti/senti.test"

fout = open(training_file, 'w')

files = os.listdir(directory)

for file in files:
    if not (file.startswith(".") | file.startswith("TRAININGFILE")):

        # Read file with encoding (ignores the char it cannot read)
        f = codecs.open(directory + "/" + file, "r", encoding='ISO-8859-1')

        lines = f.readlines()
        content = "" + file.split('.')[0]
        #content = "POS"

        # Read lines from file
        for line in lines:
            words = re.findall(r'\w+', line)
            for word in words:
                content += " " + word
            #content += " " + line.rstrip().replace("'", "")
        f.close()

        # Write to the training file
        fout.write(content + "\n")
fout.close()