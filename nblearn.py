""" Generates the model for the given training data for the Naive Bayes Classifier """
__author__ = 'Shank'

import re
import math
import sys
import copy


# To get the learning file format (key=value)
def printable_string(word_dict):
    return "".join('{}={}\n'.format(k, v) for k, v in word_dict.items())

# Read training data
training_file = open(sys.argv[1], 'r')

# Model file path
model_file = open(sys.argv[2], 'w')

class_type = ""
class_count = dict()
class_word_count = dict()
word_count = dict()
vocab_count = set()
classes = set()
num = 0

lines = training_file.readlines()
training_file.close()

for line in lines:
    #words = re.findall('[A-Z]{2,}(?![a-z])|[A-Z][a-z]+(?=[A-Z])|[\'\w\-]+', line.lower())
    words = re.findall(r'\w+', line.lower())

    class_type = words[0].upper()
    words.remove(words[0])

    classes.add(class_type)

    # Calculate the number of sentences belonging to a class
    if class_type + ".sentCount" in class_count:
        class_count[class_type + ".sentCount"] += 1
    else:
        class_count[class_type + ".sentCount"] = 1

    # Calculate the number of words in all the sentences of each class
    if class_type + ".wordCount" in class_word_count:
        class_word_count[class_type + ".wordCount"] += len(words)
    else:
        class_word_count[class_type + ".wordCount"] = len(words)

    # Calculate the word count of a class
    for word in words:
        vocab_count.add(word)
        if class_type + "." + word in word_count:
            word_count[class_type + "." + word] += 1
        else:
            word_count[class_type + "." + word] = 1

for word in word_count:
    cls = str(word).split(".")[0]
    word_count[word] = math.log((word_count[word] + 1)
                                / (class_word_count[cls + ".wordCount"]
                                   + len(vocab_count) + 1))

for cls in classes:
    word_count[cls + ".unknown"] = math.log(1 / (class_word_count[cls + ".wordCount"]
                                                 + len(vocab_count) + 1))

for cls in class_count:
    class_count[cls] = math.log(class_count[cls] / len(lines))

# Generate model file content
learning = printable_string(word_count)
learning += printable_string(class_word_count)
learning += printable_string(class_count)
learning += "classes=" + ",".join(classes)
learning += "\nlines.count=" + str(len(lines))
learning += "\nvocab.count=" + str(len(vocab_count))

# Generate model file
model_file.write(learning)
model_file.close()